# translation of kpat.po to Punjabi
#
# Amanpreet Singh Alam <aalam@redhat.com>, 2004, 2005.
# A S Alam <aalam@users.sf.net>, 2007, 2014.
msgid ""
msgstr ""
"Project-Id-Version: kpat\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-13 00:44+0000\n"
"PO-Revision-Date: 2014-03-29 18:30-0500\n"
"Last-Translator: A S Alam <aalam@users.sf.net>\n"
"Language-Team: Punjabi/Panjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "aalam@users.sf.net"

#: bakersdozen.cpp:68 castle.cpp:357 freecell.cpp:343
#, kde-format
msgid "Popular Variant Presets"
msgstr ""

#: bakersdozen.cpp:69 bakersdozen.cpp:237 bakersdozen.cpp:239
#, kde-format
msgid "Baker's Dozen"
msgstr ""

#: bakersdozen.cpp:70 bakersdozen.cpp:240
#, fuzzy, kde-format
#| msgid "KPatience"
msgid "Spanish Patience"
msgstr "ਕੇ-ਪੇਟੀਸ"

#: bakersdozen.cpp:71 bakersdozen.cpp:241
#, kde-format
msgid "Castles in Spain"
msgstr ""

#: bakersdozen.cpp:72 bakersdozen.cpp:242
#, kde-format
msgid "Portuguese Solitaire"
msgstr ""

#: bakersdozen.cpp:73 castle.cpp:364 freecell.cpp:349
#, kde-format
msgid "Custom"
msgstr ""

#: bakersdozen.cpp:75 castle.cpp:366 freecell.cpp:351
#, kde-format
msgid "Empty Stack Fill"
msgstr ""

#: bakersdozen.cpp:76 castle.cpp:367 freecell.cpp:352
#, fuzzy, kde-format
#| msgid "1 Suit (Easy)"
msgid "Any (Easy)"
msgstr "1 ਸੂਟ (ਸੌਖੀ)"

#: bakersdozen.cpp:77 castle.cpp:368 freecell.cpp:353
#, fuzzy, kde-format
#| msgid "2 Suits (Medium)"
msgid "Kings only (Medium)"
msgstr "2 ਸੂਟ (ਮੱਧਮ)"

#: bakersdozen.cpp:78 castle.cpp:369 freecell.cpp:354
#, kde-format
msgid "None (Hard)"
msgstr ""

#: bakersdozen.cpp:80
#, fuzzy, kde-format
#| msgid "Spider &Options"
msgid "Stack Options"
msgstr "ਮਕੜੀ ਚੋਣਾਂ(&O)"

#: bakersdozen.cpp:81
#, kde-format
msgid "Face &Up (Easier)"
msgstr ""

#: bakersdozen.cpp:82
#, kde-format
msgid "Face &Down (Harder)"
msgstr ""

#: bakersdozen.cpp:84 castle.cpp:371 freecell.cpp:356
#, kde-format
msgid "Build Sequence"
msgstr ""

#: bakersdozen.cpp:85 castle.cpp:372 freecell.cpp:357
#, kde-format
msgid "Alternating Color"
msgstr ""

#: bakersdozen.cpp:86 castle.cpp:373 freecell.cpp:358
#, kde-format
msgid "Matching Suit"
msgstr ""

#: bakersdozen.cpp:87 castle.cpp:374 freecell.cpp:359
#, kde-format
msgid "Rank"
msgstr ""

#: bakersdozen.cpp:243
msgid "Baker's Dozen (Custom)"
msgstr ""

#: castle.cpp:318
msgid "Castle"
msgstr ""

#: castle.cpp:320 castle.cpp:358
#, kde-format
msgid "Beleaguered Castle"
msgstr ""

#: castle.cpp:321 castle.cpp:359
#, kde-format
msgid "Citadel"
msgstr ""

#: castle.cpp:322 castle.cpp:360
#, kde-format
msgid "Exiled Kings"
msgstr ""

#: castle.cpp:323 castle.cpp:361
#, kde-format
msgid "Streets and Alleys"
msgstr ""

#: castle.cpp:324 castle.cpp:362
#, kde-format
msgid "Siegecraft"
msgstr ""

#: castle.cpp:325 castle.cpp:363
#, kde-format
msgid "Stronghold"
msgstr ""

#: castle.cpp:326
#, fuzzy
#| msgid "Freecell"
msgid "Castle (Custom)"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: castle.cpp:376
#, fuzzy, kde-format
#| msgid "Freecell"
msgid "Free Cells"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: castle.cpp:377 freecell.cpp:362
#, kde-format
msgid "0"
msgstr ""

#: castle.cpp:378 freecell.cpp:363 freecell.cpp:382
#, kde-format
msgid "1"
msgstr ""

#: castle.cpp:379 freecell.cpp:364 freecell.cpp:383
#, kde-format
msgid "2"
msgstr ""

#: castle.cpp:380 freecell.cpp:365 freecell.cpp:384
#, kde-format
msgid "3"
msgstr ""

#: castle.cpp:381 freecell.cpp:366
#, kde-format
msgid "4"
msgstr ""

#: castle.cpp:383 freecell.cpp:372
#, fuzzy, kde-format
#| msgid "Spider &Options"
msgid "Stacks"
msgstr "ਮਕੜੀ ਚੋਣਾਂ(&O)"

#: castle.cpp:384 freecell.cpp:368 freecell.cpp:373
#, kde-format
msgid "6"
msgstr ""

#: castle.cpp:385 freecell.cpp:369 freecell.cpp:374
#, kde-format
msgid "7"
msgstr ""

#: castle.cpp:386 freecell.cpp:370 freecell.cpp:375
#, kde-format
msgid "8"
msgstr ""

#: castle.cpp:387 freecell.cpp:376
#, kde-format
msgid "9"
msgstr ""

#: castle.cpp:388 freecell.cpp:377
#, kde-format
msgid "10"
msgstr ""

#: castle.cpp:390 spider.cpp:146
#, fuzzy, kde-format
#| msgid "Spider &Options"
msgid "S&tack Options"
msgstr "ਮਕੜੀ ਚੋਣਾਂ(&O)"

#: castle.cpp:391
#, kde-format
msgid "Face &Down"
msgstr ""

#: castle.cpp:392
#, kde-format
msgid "Face &Up"
msgstr ""

#: castle.cpp:393
#, kde-format
msgid "Alternating Face &Up"
msgstr ""

#: castle.cpp:395
#, kde-format
msgid "Foundation Deal"
msgstr ""

#: castle.cpp:396
#, kde-format
msgid "None"
msgstr ""

#: castle.cpp:397
#, fuzzy, kde-format
#| msgid "Aces Up"
msgid "Aces"
msgstr "ਯੱਕੇ ਉੱਤੇ"

#: castle.cpp:398
#, kde-format
msgid "Any"
msgstr ""

#: castle.cpp:400
#, kde-format
msgid "Layout"
msgstr ""

#: castle.cpp:401
#, kde-format
msgid "Classic"
msgstr ""

#: castle.cpp:402
#, kde-format
msgid "Modern"
msgstr ""

#: clock.cpp:157
msgid "Grandfather's Clock"
msgstr "ਦਾਦੇ ਦੀ ਘੜੀ"

#: dealer.cpp:72
#, kde-format
msgid "Solver: This game is winnable."
msgstr "ਹੱਲਕਰਤਾ: ਇਹ ਖੇਡ ਜਿੱਤਣਯੋਗ ਹੈ।"

#: dealer.cpp:74
#, kde-format
msgid "Solver: This game is no longer winnable."
msgstr "ਹੱਲਕਰਤਾ: ਇਹ ਖੇਡ ਜਿੱਤਣਯੋਗ ਨਹੀਂ ਰਹੀ ਹੈ।"

#: dealer.cpp:74
#, kde-format
msgid "Solver: This game cannot be won."
msgstr "ਹੱਲਕਰਤਾ: ਇਹ ਖੇਡ ਜਿੱਤੀ ਨਹੀਂ ਜਾ ਸਕਦੀ ਹੈ।"

#: dealer.cpp:76
#, kde-format
msgid "Solver: Unable to determine if this game is winnable."
msgstr ""

#: dealer.cpp:932
#, kde-format
msgid "Congratulations! We have won."
msgstr "ਮੁਬਾਰਕ! ਅਸੀਂ ਜਿੱਤ ਗਏ ਹਾਂ!"

#: dealer.cpp:934
#, kde-format
msgid "Congratulations! You have won."
msgstr "ਮੁਬਾਰਕ! ਤੁਸੀਂ ਜਿੱਤ ਗਏ ਹੋ!"

#: dealer.cpp:1220
#, kde-format
msgid "Solver: This game is lost."
msgstr "ਹੱਲਕਰਤਾ:ਇਹ ਖੇਡ ਹਾਰੀ ਗਈ।"

#: dealer.cpp:1358
#, kde-format
msgid "Solver: Calculating..."
msgstr "ਹੱਲਕਰਤਾ:...ਗਿਣਤੀ ਕੀਤੀ ਜਾ ਰਹੀ ਹੈ"

#: dealer.cpp:1762
#, kde-format
msgid ""
"A new game has been requested, but there is already a game in progress.\n"
"\n"
"A loss will be recorded in the statistics if the current game is abandoned."
msgstr ""

#: dealer.cpp:1764
#, kde-format
msgid "Abandon Current Game?"
msgstr "ਕੀ ਮੌਜੂਦਾ ਖੇਡ ਛੱਡਣੀ ਹੈ?"

#: dealer.cpp:1765
#, kde-format
msgid "Abandon Current Game"
msgstr "ਮੌਜੂਦਾ ਖੇਡ ਵਿਚੇ ਛੱਡੋ"

#: fortyeight.cpp:229
msgid "Forty & Eight"
msgstr "ਚਾਲੀ ਅਤੇ ਅੱਠ"

#: freecell.cpp:307 freecell.cpp:312 freecell.cpp:347
#, kde-format
msgid "Freecell"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: freecell.cpp:309 freecell.cpp:344
#, kde-format
msgid "Baker's Game"
msgstr ""

#: freecell.cpp:310 freecell.cpp:345
#, kde-format
msgid "Eight Off"
msgstr ""

#: freecell.cpp:311 freecell.cpp:346
#, fuzzy, kde-format
#| msgid "Freecell"
msgid "Forecell"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: freecell.cpp:313 freecell.cpp:348
#, kde-format
msgid "Seahaven Towers"
msgstr ""

#: freecell.cpp:314
#, fuzzy
#| msgid "Freecell"
msgid "Freecell (Custom)"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: freecell.cpp:361
#, fuzzy, kde-format
#| msgid "Freecell"
msgid "Free cells"
msgstr "ਖਾਲੀ ਸੈੱਲ"

#: freecell.cpp:367
#, kde-format
msgid "5"
msgstr ""

#: freecell.cpp:378
#, kde-format
msgid "11"
msgstr ""

#: freecell.cpp:379
#, kde-format
msgid "12"
msgstr ""

#: freecell.cpp:381
#, kde-format
msgid "Decks"
msgstr ""

#: golf.cpp:189
msgid "Golf"
msgstr "ਗੋਲਫ"

#: grandf.cpp:188
msgid "Grandfather"
msgstr "ਦਾਦਾ"

#: gypsy.cpp:166
msgid "Gypsy"
msgstr "ਜਿਪਸੀ"

#: idiot.cpp:225
msgid "Aces Up"
msgstr "ਯੱਕੇ ਉੱਤੇ"

#: klondike.cpp:134
#, kde-format
msgid "Klondike &Options"
msgstr "ਕਲੋਡੀਕ ਚੋਣਾਂ"

#: klondike.cpp:135
#, kde-format
msgid "Draw 1"
msgstr "ਡਰਾਅ 1"

#: klondike.cpp:136
#, kde-format
msgid "Draw 3"
msgstr "ਡਰਾਅ 3"

#: klondike.cpp:312
msgid "Klondike"
msgstr "ਕਲੋਡੀਕ"

#: klondike.cpp:314
msgid "Klondike (Draw 1)"
msgstr "ਕਲੋਡੀਕ (ਡਰਾਅ 1)"

#: klondike.cpp:315
msgid "Klondike (Draw 3)"
msgstr "ਕਲੋਡੀਕ (ਡਰਾਅ 3)"

#. i18n: ectx: Menu (move)
#: kpatui.rc:23
#, kde-format
msgid "Move"
msgstr "ਚਾਲ"

#. i18n: ectx: ToolBar (mainToolBar)
#: kpatui.rc:43
#, kde-format
msgid "Main Toolbar"
msgstr "ਮੁੱਖ ਸੰਦ-ਪੱਟੀ"

#: libkcardgame/kcardthemewidget.cpp:266
#, kde-format
msgid "Loading..."
msgstr "...ਲੋਡ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ"

#: main.cpp:92
#, kde-format
msgid "KPatience"
msgstr "ਕੇ-ਪੇਟੀਸ"

#: main.cpp:94
#, kde-format
msgid "KDE Patience Game"
msgstr "KDE ਪੇਟੀਸ ਖੇਡ"

#: main.cpp:96
#, kde-format
msgid ""
"© 1995 Paul Olav Tvete\n"
"© 2000 Stephan Kulow"
msgstr ""
"© 1995 Paul Olav Tvete\n"
"© 2000 Stephan Kulow"

#: main.cpp:100
#, kde-format
msgid "Paul Olav Tvete"
msgstr ""

#: main.cpp:100
#, kde-format
msgid "Author of original Qt version"
msgstr ""

#: main.cpp:101
#, kde-format
msgid "Mario Weilguni"
msgstr ""

#: main.cpp:101
#, kde-format
msgid "Initial KDE port"
msgstr ""

#: main.cpp:102
#, kde-format
msgid "Matthias Ettrich"
msgstr ""

#: main.cpp:103
#, kde-format
msgid "Rodolfo Borges"
msgstr ""

#: main.cpp:103 main.cpp:116
#, kde-format
msgid "New game types"
msgstr "ਨਵੀਆਂ ਖੇਡ ਕਿਸਮਾਂ"

#: main.cpp:104
#, kde-format
msgid "Peter H. Ruegg"
msgstr ""

#: main.cpp:105
#, kde-format
msgid "Michael Koch"
msgstr ""

#: main.cpp:105 main.cpp:112
#, kde-format
msgid "Bug fixes"
msgstr "ਬੱਗ ਨਿਰਧਾਰਨ"

#: main.cpp:106
#, kde-format
msgid "Marcus Meissner"
msgstr ""

#: main.cpp:106
#, kde-format
msgid "Shuffle algorithm for game numbers"
msgstr "ਖੇਡ ਅੰਕ ਲਈ ਰਲਾਉਣ ਲਈ ਐਲਗੋਰਿਥਮ"

#: main.cpp:107
#, kde-format
msgid "Tom Holroyd"
msgstr ""

#: main.cpp:107
#, kde-format
msgid "Initial patience solver"
msgstr ""

#: main.cpp:108
#, kde-format
msgid "Stephan Kulow"
msgstr ""

#: main.cpp:108
#, kde-format
msgid "Rewrite and current maintainer"
msgstr "ਲੇਖਕ ਤੇ ਮੌਜੂਦਾ ਪ੍ਰਬੰਧਕ"

#: main.cpp:109
#, kde-format
msgid "Erik Sigra"
msgstr ""

#: main.cpp:109
#, kde-format
msgid "Klondike improvements"
msgstr "ਕਲੋਡੀਕ ਸੁਧਾਰ"

#: main.cpp:110
#, kde-format
msgid "Josh Metzler"
msgstr ""

#: main.cpp:110
#, kde-format
msgid "Spider implementation"
msgstr "ਮੱਕੜੀ ਸਥਾਪਨ"

#: main.cpp:111
#, kde-format
msgid "Maren Pakura"
msgstr ""

#: main.cpp:111
#, kde-format
msgid "Documentation"
msgstr "ਦਸਤਾਵੇਜ਼"

#: main.cpp:112
#, kde-format
msgid "Inge Wallin"
msgstr ""

#: main.cpp:113
#, kde-format
msgid "Simon Hürlimann"
msgstr ""

#: main.cpp:113
#, kde-format
msgid "Menu and toolbar work"
msgstr "ਮੇਨੂ ਅਤੇ ਸੰਦ-ਪੱਟੀ ਕੰਮ"

#: main.cpp:114
#, kde-format
msgid "Parker Coates"
msgstr ""

#: main.cpp:114
#, kde-format
msgid "Cleanup and polish"
msgstr ""

#: main.cpp:115
#, kde-format
msgid "Shlomi Fish"
msgstr ""

#: main.cpp:115
#, kde-format
msgid "Integration with Freecell Solver and further work"
msgstr ""

#: main.cpp:116
#, kde-format
msgid "Michael Lang"
msgstr ""

#: main.cpp:123
#, kde-format
msgid "Try to find a solution to the given savegame"
msgstr ""

#: main.cpp:124
#, kde-format
msgid "Dealer to solve (debug)"
msgstr ""

#: main.cpp:125
#, kde-format
msgid "Game range start (default 0:INT_MAX)"
msgstr ""

#: main.cpp:127
#, kde-format
msgid "Game range end (default start:start if start given)"
msgstr ""

#: main.cpp:128
#, kde-format
msgid "Directory with test cases"
msgstr ""

#: main.cpp:129
#, kde-format
msgid "Generate random test cases"
msgstr ""

#: main.cpp:130
#, kde-format
msgid "File to load"
msgstr "ਲੋਡ ਕਰਨ ਲ਼ਈ ਫਾਇਲ"

#: mainwindow.cpp:152
#, kde-format
msgctxt "Start a new game of a different type"
msgid "New &Game..."
msgstr "ਨਵੀਂ ਖੇਡ(&G)..."

#: mainwindow.cpp:158
#, kde-format
msgctxt "Start a new game of without changing the game type"
msgid "New &Deal"
msgstr "ਨਵੀਂ ਡੀਲ(&D)"

#: mainwindow.cpp:164
#, kde-format
msgctxt "Start a game by giving its particular number"
msgid "New &Numbered Deal..."
msgstr "...ਅੰਕੀ ਖੇਡ ਚੁਣੋ(&N)"

#: mainwindow.cpp:169
#, kde-format
msgctxt "Replay the current deal from the start"
msgid "Restart Deal"
msgstr "ਡੀਲ ਮੁੜ-ਚਾਲੂ"

#: mainwindow.cpp:175
#, kde-format
msgctxt "Start the game with the number one greater than the current one"
msgid "Next Deal"
msgstr ""

#: mainwindow.cpp:184
#, kde-format
msgctxt "Start the game with the number one less than the current one"
msgid "Previous Deal"
msgstr ""

#. i18n: ectx: property (windowTitle), widget (QWidget, GameStats)
#: mainwindow.cpp:198 statisticsdialog.cpp:61 statisticsdialog.ui:13
#, kde-format
msgid "Statistics"
msgstr "ਅੰਕੜੇ"

#: mainwindow.cpp:227
#, kde-format
msgctxt ""
"Take one or more cards from the deck, flip them, and place them in play"
msgid "Dra&w"
msgstr "ਡਰਾਅ(&w)"

#: mainwindow.cpp:232
#, kde-format
msgctxt "Deal a new row of cards from the deck"
msgid "Dea&l Row"
msgstr ""

#: mainwindow.cpp:237
#, kde-format
msgctxt "Collect the cards in play, shuffle them and redeal them"
msgid "&Redeal"
msgstr "ਮੁੜ ਵੰਡੋ(&R)"

#: mainwindow.cpp:242
#, kde-format
msgctxt "Automatically move cards to the foundation piles"
msgid "Dro&p"
msgstr "ਛੱਡੋ(&p)"

#: mainwindow.cpp:250
#, kde-format
msgid "Change Appearance..."
msgstr "...ਦਿੱਖ ਬਦਲੋ"

#: mainwindow.cpp:254
#, kde-format
msgid "&Enable Autodrop"
msgstr "ਸਵੈ-ਚਾਲਿਤ ਪੱਤੇ ਚੱਕਣਾ ਯੋਗ(&E)"

#: mainwindow.cpp:259
#, kde-format
msgid "E&nable Solver"
msgstr "ਹੱਲਕਰਤਾ ਯੋਗ(&n)"

#: mainwindow.cpp:264
#, kde-format
msgid "Play &Sounds"
msgstr "ਆਵਾਜ਼ਾ ਚਲਾਓ(&S)"

#: mainwindow.cpp:269
#, kde-format
msgid "&Remember State on Exit"
msgstr "ਬੰਦ ਕਰਨ ਸਮੇਂ ਸਥਿਤੀ ਯਾਦ ਰੱਖੋ(&R)"

#: mainwindow.cpp:284
#, kde-format
msgid "Generate a theme preview image"
msgstr ""

#: mainwindow.cpp:289
#, kde-format
msgid "Take Game Preview Snapshots"
msgstr ""

#: mainwindow.cpp:294
#, kde-format
msgid "Random Cards"
msgstr "ਰਲਵੇਂ ਕਾਰਡ"

#: mainwindow.cpp:301
#, kde-format
msgid "Move Focus to Previous Pile"
msgstr ""

#: mainwindow.cpp:305
#, kde-format
msgid "Move Focus to Next Pile"
msgstr ""

#: mainwindow.cpp:309
#, kde-format
msgid "Move Focus to Card Below"
msgstr ""

#: mainwindow.cpp:313
#, kde-format
msgid "Move Focus to Card Above"
msgstr ""

#: mainwindow.cpp:317
#, kde-format
msgid "Cancel Focus"
msgstr ""

#: mainwindow.cpp:321
#, kde-format
msgid "Pick Up or Set Down Focus"
msgstr ""

#: mainwindow.cpp:429
#, kde-format
msgid "Card Deck"
msgstr "ਕਾਰਡ ਡੈਕ"

#: mainwindow.cpp:431
#, kde-format
msgid "Select a card deck"
msgstr "ਕਾਰਡ ਡੈਕ ਚੁਣੋ"

#: mainwindow.cpp:436
#, kde-format
msgid "Game Theme"
msgstr "ਖੇਡ ਥੀਮ"

#: mainwindow.cpp:436
#, kde-format
msgid "Select a theme for non-card game elements"
msgstr ""

#: mainwindow.cpp:514
#, kde-format
msgctxt ""
"Is disabled and changes to \"Help &with Current Game\" when there is no "
"current game."
msgid "Help &with %1"
msgstr "%1 ਨਾਲ ਮੱਦਦ(&w)"

#: mainwindow.cpp:545
#, kde-format
msgctxt "Shown when there is no game open. Is always disabled."
msgid "Help &with Current Game"
msgstr "ਮੌਜੂਦਾ ਖੇਡ ਲਈ ਮੱਦਦ(&w)"

#: mainwindow.cpp:700
#, kde-format
msgid ""
"Are you sure you want to hide the menubar? The current shortcut to show it "
"again is %1."
msgstr ""

#: mainwindow.cpp:702
#, kde-format
msgid "Hide Menubar"
msgstr "ਮੇਨੂ-ਪੱਟੀ ਓਹਲੇ"

#: mainwindow.cpp:795
#, fuzzy, kde-format
#| msgid "Downloading file failed."
msgid "Downloading file failed: %1"
msgstr "ਫਾਇਲ ਡਾਊਨਲੋਡ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ।"

#: mainwindow.cpp:801
#, kde-format
msgid "Error reading XML file: "
msgstr "XML ਫਾਇਲ ਪੜ੍ਹਨ ਵਿੱਚ ਗਲਤੀ: "

#: mainwindow.cpp:818
#, kde-format
msgid "XML file is not a KPat save."
msgstr ""

#: mainwindow.cpp:823
#, kde-format
msgid "Unrecognized game id."
msgstr ""

#: mainwindow.cpp:842
#, kde-format
msgid "Errors encountered while parsing file."
msgstr ""

#: mainwindow.cpp:861
#, kde-format
msgid "Load"
msgstr "ਲੋਡ"

#: mainwindow.cpp:888
#, kde-format
msgid "Save"
msgstr "ਸੰਭਾਲੋ"

#: mainwindow.cpp:904
#, kde-format
msgid "Error opening file for writing. Saving failed."
msgstr ""

#: mainwindow.cpp:909
#, kde-format
msgid "Unable to create temporary file. Saving failed."
msgstr ""

#: mainwindow.cpp:925
#, fuzzy, kde-format
#| msgid "Downloading file failed."
msgid "Error uploading file. Saving failed: %1"
msgstr "ਫਾਇਲ ਡਾਊਨਲੋਡ ਕਰਨ ਲਈ ਫੇਲ੍ਹ ਹੈ।"

#: mainwindow.cpp:949
#, kde-format
msgid "1 move"
msgid_plural "%1 moves"
msgstr[0] "1 ਚਾਲ"
msgstr[1] "%1 ਚਾਲਾਂ"

#: mod3.cpp:218
msgid "Mod3"
msgstr "Mod3"

#: numbereddealdialog.cpp:36
#, kde-format
msgid "New Numbered Deal"
msgstr "ਨਵੀਂ ਅੰਕੀ ਡੀਲ"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: numbereddealdialog.cpp:51 statisticsdialog.ui:33
#, kde-format
msgid "Game:"
msgstr "ਖੇਡ:"

#: numbereddealdialog.cpp:55
#, kde-format
msgid "Deal number:"
msgstr "ਡੀਲ ਅੰਕ:"

#: simon.cpp:168
msgid "Simple Simon"
msgstr "ਸਧਾਰਨ ਸੀਮੋਨ"

#: spider.cpp:131
#, kde-format
msgid "Spider &Options"
msgstr "ਮਕੜੀ ਚੋਣਾਂ(&O)"

#: spider.cpp:132
#, kde-format
msgid "1 Suit (Easy)"
msgstr "1 ਸੂਟ (ਸੌਖੀ)"

#: spider.cpp:133
#, kde-format
msgid "2 Suits (Medium)"
msgstr "2 ਸੂਟ (ਮੱਧਮ)"

#: spider.cpp:134
#, fuzzy, kde-format
#| msgid "4 Suits (Hard)"
msgid "3 Suits (Hard)"
msgstr "4 ਸੂਟ (ਔਖੀ)"

#: spider.cpp:135
#, fuzzy, kde-format
#| msgid "4 Suits (Hard)"
msgid "4 Suits (Very Hard)"
msgstr "4 ਸੂਟ (ਔਖੀ)"

#: spider.cpp:147
#, kde-format
msgid "Face &Down (harder)"
msgstr ""

#: spider.cpp:148
#, kde-format
msgid "Face &Up (easier)"
msgstr ""

#: spider.cpp:466
msgid "Spider"
msgstr "ਮੱਕੜੀ"

#: spider.cpp:468
msgid "Spider (1 Suit)"
msgstr "ਮੱਕੜੀ (1 ਸੂਟ)"

#: spider.cpp:469
msgid "Spider (2 Suit)"
msgstr "ਮੱਕੜੀ (2 ਸੂਟ)"

#: spider.cpp:470
#, fuzzy
#| msgid "Spider (1 Suit)"
msgid "Spider (3 Suit)"
msgstr "ਮੱਕੜੀ (1 ਸੂਟ)"

#: spider.cpp:471
msgid "Spider (4 Suit)"
msgstr "ਮੱਕੜੀ (4 ਸੂਟ)"

#: statisticsdialog.cpp:122
#, kde-format
msgid "%1 (%2%)"
msgstr "%1 (%2%)"

#: statisticsdialog.cpp:134
#, kde-format
msgid "1 loss"
msgid_plural "%1 losses"
msgstr[0] "1 ਹਾਰੇ"
msgstr[1] "%1 ਹਾਰੇ"

#: statisticsdialog.cpp:136
#, kde-format
msgid "1 win"
msgid_plural "%1 wins"
msgstr[0] "ਜਿੱਤ"
msgstr[1] "%1 ਜਿੱਤਾਂ"

#. i18n: ectx: property (text), widget (QLabel, textLabel7)
#: statisticsdialog.ui:75
#, kde-format
msgid "Longest winning streak:"
msgstr "ਸਭ ਤੋਂ ਲੰਮ ਜਿੱਤ ਸਟਰੀਕ:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: statisticsdialog.ui:82
#, kde-format
msgid "Games played:"
msgstr "ਖੇਡੀਆਂ ਖੇਡਾਂ:"

#. i18n: ectx: property (text), widget (QLabel, textLabel8)
#: statisticsdialog.ui:99
#, kde-format
msgid "Longest losing streak:"
msgstr "ਸਭ ਤੋਂ ਲੰਮਾ ਹਾਰ ਸਟਰੀਕ:"

#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: statisticsdialog.ui:116
#, kde-format
msgid "Games won:"
msgstr "ਜਿੱਤੀਆਂ ਖੇਡਾਂ:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: statisticsdialog.ui:133
#, kde-format
msgid "Current streak:"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: statisticsdialog.ui:147
#, kde-format
msgid "Minimal number of moves:"
msgstr ""

#: yukon.cpp:120
msgid "Yukon"
msgstr "ਯੂਕੋਨ"

#~ msgctxt "List separator"
#~ msgid ", "
#~ msgstr ", "

#~ msgid "Get New Card Decks..."
#~ msgstr "...ਨਵੇਂ ਕਾਰਡ ਡੈਕ ਲਵੋ"

#~ msgid "Opening file failed."
#~ msgstr "ਫਾਇਲ ਖੋਲ੍ਹਣ ਲਈ ਫੇਲ੍ਹ ਹੈ।"

#, fuzzy
#~| msgid "The saved game is of unknown type!"
#~ msgid "The saved game is of an unknown type."
#~ msgstr "ਤੁਸੀਂ ਅਣਜਾਣੀ ਕਿਸਮ ਦੀ ਖੇਡ ਸੰਭਾਲੀ ਹੈ!"

#, fuzzy
#~| msgid ""
#~| "Enter a game number (FreeCell deals are the same as in the FreeCell FAQ):"
#~ msgid ""
#~ "Enter a deal number (Freecell deals are numbered the same as those in the "
#~ "Freecell FAQ):"
#~ msgstr ""
#~ "ਇੱਕ ਖੇਡ ਅੰਕ ਦਿਓ (ਮੁਕਤ ਥਾਂ ਉਸਤਰਾਂ ਹੀ ਵੰਡੀ ਜਾਦੀ ਹੈ, ਜਿਸ ਤਰਾਂ ਮੁਕਤ-ਥਾਂ ਸਵਾਲ ਜਵਾਬ ਵਿੱਚ):"

#~ msgid "Improved Klondike"
#~ msgstr "ਸੋਧੀ ਕਲੋਡੀਕ"

#, fuzzy
#~| msgid "This game is lost."
#~ msgid "This game is lost. No moves remain."
#~ msgstr "ਇਹ ਖੇਡ ਹਾਰੀ ਗਈ।"

#, fuzzy
#~| msgid ""
#~| "You are already running an unfinished game.  If you abort the old game "
#~| "to start a new one, the old game will be registered as a loss in the "
#~| "statistics file.\n"
#~| "What do you want to do?"
#~ msgid ""
#~ "You are already running an unfinished game. If you abort the old game to "
#~ "start a new one, the old game will be registered as a loss in the "
#~ "statistics file.\n"
#~ "What do you want to do?"
#~ msgstr ""
#~ "ਤੁਸੀਂ ਇੱਕ ਨਾ-ਖਤਮ ਹੋਈ ਖੇਡ ਪਹਿਲਾਂ ਹੀ ਚਲਾ ਰਹੇ ਹੋ।  ਜੇਕਰ ਤੁਸੀਂ ਪੁਰਾਣੀ ਖੇਡ ਨੂੰ ਨਵੀਂ ਸ਼ੁਰੂ ਕਰਨ ਲਈ "
#~ "ਛੱਡਣਾ ਚਾਹੁੰਦੇ ਹੋ ਤਾਂ ਪੁਰਾਣੀ ਖੇਡ ਨੂੰ ਹਾਰੀ ਹੋਈ ਦੇ ਤੌਰ 'ਤੇ ਅੰਕੜਾ ਫਾਇਲ 'ਚ ਰਜਿਸਟਰ ਕਰ ਲਿਆ "
#~ "ਜਾਵੇਗਾ।\n"
#~ "ਤੁਸੀਂ ਕੀ ਕਰਨਾ ਚਾਹੋਗੇ?"

#~ msgid "&Settings"
#~ msgstr "ਸੈਟਿੰਗ(&S)"

#, fuzzy
#~| msgid "&Redeal"
#~ msgid "&Reset"
#~ msgstr "ਮੁੜ ਵੰਡੋ(&R)"

#~ msgid "&OK"
#~ msgstr "ਠੀਕ ਹੈ(&O)"

#~ msgid "&Calculation"
#~ msgstr "ਗਣਿਤ(&C)"

#~ msgid "&Napoleon's Tomb"
#~ msgstr "ਨਪੋਲੀਅਨ ਟੋਮਬ(&N)"

#~ msgid "Disable Autodrop"
#~ msgstr "ਸਵੈ-ਚਾਲਿਤ ਪੱਤੇ ਚੱਕਣਾ ਅਯੋਗ"
